/**
 * Concentric
 * A visual clock
 *
 * Inspired by & based on:
 * https://codepen.io/motorlatitude/pen/uevDx
 */

const clock = document.getElementById('c');
const ctx = c.getContext('2d');
const size = 500;
const half = size / 2;
const lineWidth = 3;
const linePadding = 29;
const startAngle = Math.PI / (2 / 3);

clock.width = size;
clock.height = size;

Object.assign(ctx, {
  lineWidth,
  textAlign: 'center',
  font: '10px monospace'
});

function Aequirys (d = new Date()) {
  const first = new Date(d.getFullYear(), 0, 1);
  const n = ~~((d - first) / 864E5) + 1;
  let date = 0;
  let month = 0;

  switch (n) {
    case 365: case 366: break;
    default: {
      date = n - 14 * ~~(n / 14);
      if (date === 0) date = 14;
      month = ~~((n - 1) / 14);
      break;
    }
  }

  return {month, date};
}

function time () {
  const d = new Date();
  const t = (new Date(d) - d.setHours(0, 0, 0, 0)) / 864E5;
  const f = t.toFixed(6).substr(2, 6);
  return [f.substr(0, 3), f.substr(3, 3)];
}

function calcAngle (x, y) {
  return 1.57 * (4 * x - y) / y;
}

function Arc (nthPosition, maxValue) {
  this.maxValue = maxValue;
  this.radius = (lineWidth + linePadding) * nthPosition;
  this.draw = function (x, h = half, s = startAngle) {
    const angle = calcAngle(x, this.maxValue);
    ctx.beginPath();
    ctx.arc(h, h, this.radius, s, angle, false);
    ctx.stroke();
  };
}

function display (month, date, beat, pulse) {
  const mon = String.fromCharCode(97 + month).toUpperCase();
  const str = `${`0${date}`.substr(-2)}${mon} ${beat}:${pulse}`;
  ctx.fillText(str, half, size - 60);
}

const arcMonth = new Arc(1, 26);
const arcDate = new Arc(2, 14);
const arcBeat = new Arc(3, 1E3);
const arcPulse = new Arc(4, 1E3);

function draw (month, date, beat, pulse) {
  ctx.clearRect(0, 0, size, size);
  arcMonth.draw(month);
  arcDate.draw(date);
  arcBeat.draw(beat);
  arcPulse.draw(pulse);
}

function paint (bg, fg) {
  document.body.style.backgroundColor = bg;
  Object.assign(ctx, {fillStyle: fg, strokeStyle: fg});
}

function invert () {
  const bg = document.body.style.backgroundColor;
  const fg = ctx.fillStyle;
  paint(fg, bg);
}

function parseHash () {
  const {hash} = window.location;
  let BG = '#030303';
  let FG = '#f8f8f8';
  if (hash) {
    const isHex = value => /^#[0-9a-f]{3}(?:[0-9a-f]{3})?$/i.test(value);
    let [a, b] = hash.split('-');
    b = `#${b}`;
    BG = isHex(a) ? a : BG;
    FG = isHex(b) ? b : FG;
  }
  paint(BG, FG);
}

window.onhashchange = parseHash;

document.onkeyup = function ({code}) {
  if (code === 'KeyI') invert();
}

parseHash();

(function tick () {
  const {date, month} = Aequirys();
  const [beat, pulse] = time();
  ctx.save();
  draw(month, date, beat, pulse);
  display(month, date, beat, pulse);
  ctx.restore();
  window.requestAnimationFrame(tick);
}());
