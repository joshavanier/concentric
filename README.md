[![Screenshot](screenshot.png)](https://joshavanier.github.io/concentric)

**Concentric** is a clock inspired by and based on this [Pen](https://codepen.io/motorlatitude/pen/uevDx).

### To Do
- [ ] Handle Year and Leap days

---

**[Josh Avanier](https://joshavanier.github.io)**
